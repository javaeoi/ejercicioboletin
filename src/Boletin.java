import java.security.SecureRandom;
import java.time.LocalDate;
import java.util.Scanner;

public class Boletin {
    public static void main(String[] args) {

        // Pedir el alumno y la nota que se le asigna por matería
        String alumno;
        float[] calificacion;

        Scanner sc = new Scanner(System.in);
        System.out.println("¿Cuál alumno?");
        alumno = sc.nextLine();
        calificacion = notas();

        // Para separar bien el boletin de la insercion de datos
        System.out.println("");
        System.out.println("");
        System.out.println("");

        //llamo a imprimir el boletín
        sc.close();
        impresion(alumno, calificacion);

    }

    public static LocalDate fechaHoy(){
        return LocalDate.now();
    }

    public static void impresion(String alumno, float[] calificacion){
        // Función que imprime el boletín al completo
        System.out.println("Boletín de calificaciones del curso de Java – " + fechaHoy());
        Alumnos aprendiz = Alumnos.valueOf(alumno);
        System.out.println();
        System.out.println("Alumno: " + alumno);
        System.out.println(aprendiz.getNombre() + " " + aprendiz.getApellidos());
        System.out.println(aprendiz.getEmail());
        System.out.println();
        /*------------------------------------------------------------------------*/

        System.out.println("Materia                      Descripcion                     Docente                             Calificación");
        nombreMateria(calificacion);
        /*------------------------------------------------------------------------*/

        System.out.println();
        System.out.println();
        float media = mediaDeArray(calificacion);
        System.out.print("Calificación media del curso: " + media);
        notaComentario(media);
        System.out.println("");
        System.out.print("Documento firmado electronicamente: ");
        System.out.println(generateRandomString(20));

    }

    public static void matDescDoc(String materia, float calificaciones){
        // Funcion que recibe el nombre de la materia, del cual a raiz de él y empleando los getters del enumerador, sacamos el resto
        // de valores asociados y por ultimo añadimos la calificación correspondiente
        Materias asignatura = Materias.valueOf(materia);
        System.out.print( materia + "                       " + asignatura.getMateria() + "                              " + asignatura.getDocente() + "                              " + calificaciones);
        notaComentario(calificaciones);
    }

    public static void nombreMateria(float[] calificaciones){
        // Funcion que obtiene el nombre correspondiente a la columna materia y pposteriormente lo envia a la funcion matDescDoc()
        String materia;
        String materia_comienzo = "CJ0";

        for (int i = 1; i <= 12; i++){
            if (i<10){
                materia = materia_comienzo + "0" + i;
            }else{
                materia = materia_comienzo + i;
            }
            matDescDoc(materia, calificaciones[i-1]);
        }
    }

    public static float[] notas(){
        // Función para preguntar que nota a sacado el alumno por cada asignatura
        Scanner sc = new Scanner(System.in);
        float[] datos = new float[12];
        String materia;
        String materia_comienzo = "CJ0";
        String num;

        for (int i = 1; i <= 12; i++){
            if (i<10){
                materia = materia_comienzo + "0" + i;
            }else{
                materia = materia_comienzo + i;
            }
            Materias asignatura = Materias.valueOf(materia);
            System.out.println("¿Que nota tiene el alumno en la asignatura "+ asignatura.getMateria() + "?");
            num = sc.nextLine();
            datos[i-1] = Float.parseFloat(num);
        }

        return datos;
    }

    public static float mediaDeArray (float[] calificacion){
        // Recoge los números del array y calcula la nota media
        int sum = 0;
        float nota_media;

        for (float valor : calificacion) {
            sum += valor;
        }

        nota_media = (float) sum/calificacion.length;
        return nota_media;
    }

    public static void notaComentario (float num){
        // añadir un comentario al lado de la nota
        if (num >= 0 && num <=4.5)
                System.out.println(" - Insuficiente");
        if (num >= 4.5 && num<=5.99)
                System.out.println(" - Suficiente");
        if (num >= 6 && num<=7.99)
                System.out.println(" - Bien");
        if (num >= 8 && num<= 9.99)
                System.out.println(" - Notable");
        if (num == 10)
                System.out.println(" - Sobresaliente");
    }

    //Internet me dice como sacar un String de longitud variable alfanumerico
    public static String generateRandomString(int length) {
        // Puede personalizar los personajes que desea agregar a
        // las cadenas al azar
        String CHAR_LOWER = "abcdefghijklmnopqrstuvwxyz";
        String CHAR_UPPER = CHAR_LOWER.toUpperCase();
        String NUMBER = "0123456789";

        String DATA_FOR_RANDOM_STRING = CHAR_LOWER + CHAR_UPPER + NUMBER;
        SecureRandom random = new SecureRandom();

        if (length < 1) throw new IllegalArgumentException();

        StringBuilder sb = new StringBuilder(length);

        for (int i = 0; i < length; i++) {
            // 0-62 (exclusive), retornos aleatorios 0-61
            int rndCharAt = random.nextInt(DATA_FOR_RANDOM_STRING.length());
            char rndChar = DATA_FOR_RANDOM_STRING.charAt(rndCharAt);

            sb.append(rndChar);
        }

        return sb.toString();
    }

}
